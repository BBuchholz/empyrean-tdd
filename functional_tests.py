from selenium import webdriver
import unittest

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_can_start_a_list_and_retrieve_it_later(self):
        # a user navigates to the homepage where they can enter in and
        # view existing Synergy Items in a list format

        self.browser.get('http://localhost:8000')

        # they notice the page title and header mention to-do lists
        self.assertIn('Synergy', self.browser.title)
        self.fail('Finish the test!')

        # they are invited to enter a synergy item straight away

        # they type "ToDo: experiment with Synergy" into a text box 

        # When they hit enter, the page updates, and now the page lists
        # "ToDo: experiment with Synergy" as an item in a Synergy list

        # There is still a text box inviting them to add another item. They
        # enter "RandomThought: we need to start doing more TDD for NWD"

        # The page updates again, and now shows both items on their list

        # They wonder whether the site will remember their list. Then they see
        # that the site has generated a unique URL for them -- there is some
        # explanatory text to that effect.

        # They visit that URL - their Synergy list is still there.

        # Satisfied, they go about their activities

if __name__ == '__main__':
    unittest.main(warnings='ignore')
